# -*- coding: utf-8 -
import os
import logging
import git

log = logging.getLogger(__name__)


def get_info():
    try:
        root = os.path.abspath(os.path.join(
            os.path.dirname(__file__),
            '../../../'
        ))

        extensions = []
        for _, __, ___ in os.walk(root):
            for dir in __:
                if dir.startswith('ckanext'):
                    extensions.append(os.path.join(root, dir))
            break

        result = {}
        for ext in extensions:
            ext_name = ext.split('/')[-1]
            if ext_name in result.keys():
                continue
            try:
                repo = git.Repo(ext)
            except:
                # Check if submodule
                try:
                    sub = git.Submodule(ext)
                    branch, tag = None, None
                    commit = sub.parent_commit
                    result.update({ext_name: {
                        'branch': branch,
                        'commit': commit,
                        'tag': tag
                    }})
                    continue
                except:
                    continue

            branch, commit, tag = None, repo.head.commit.hexsha, None
            try:
                branch = repo.active_branch.name
            except TypeError:
                pass

            for t in repo.tags:
                if t is None:
                    continue
                if t.commit.hexsha == commit:
                    tag = t.name

            result.update({ext_name: {
                'branch': branch,
                'commit': commit,
                'tag': tag
            }})

        return result
    except Exception as e:
        log.info('Unexpected error when rendering about page for sysadmin.')
        log.debug(e)
        return {}
