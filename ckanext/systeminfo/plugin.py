# -*- coding: utf-8 -
import logging

import ckanext.systeminfo.helpers as h

log = logging.getLogger(__name__)

import ckan.plugins as p
import ckan.plugins.toolkit as t


class SysteminfoPlugin(p.SingletonPlugin):
    p.implements(p.IConfigurer)
    p.implements(p.ITemplateHelpers)

    # IConfigurer

    def update_config(self, config_):
        t.add_template_directory(config_, 'templates')
        t.add_public_directory(config_, 'public')
        t.add_resource('fanstatic', 'systeminfo')

    # ITemplateHelpers

    def get_helpers(self):
        return {
            'show_system_info': h.get_info,
        }
